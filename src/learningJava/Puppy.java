package learningJava;
//this program illustrates this pointer constructor overloading and passing parameters to function
public class Puppy {
	int age;
	String name;
	Puppy(){
		age=0;
		name="jacky";
	}
	
	Puppy(String name){
		this.name=name;
		System.out.println("Name:"+this.name);
		
	}
	void setage(int age) {
		this.age=age;
	}
	void show() {
		System.out.println("NAME:"+this.name);
		System.out.println("Age:"+this.age);
	}
	
	public static void main(String [] arg) {
		Puppy pup1=new Puppy("bruno");
		pup1.setage(5);
		Puppy pup2=new Puppy();
		pup1.show();
		pup2.show();
		System.out.println("All constructors called");
		
	}

}
