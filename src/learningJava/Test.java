package learningJava;
import java.util.Scanner;
import java.util.InputMismatchException;
import java.util.Scanner;
//class to get detail of all the actors in the movie
public class Test extends Actors {
	 
	 private static Integer actorNo;
	 private Actors act[];
	
	 public void scene() {
		Scanner in = new Scanner(System.in);
		System.out.println("Enter no of actors in this Movie");
		actorNo=getcorrectvalue();
		
		act=new Actors[actorNo];
		
		System.out.println("Enter Details of actor");
		for(int j=0;j<act.length;j++) {
			System.out.println("For Actor "+(j+1));	
			act[j]= new Actors();
		act[j].get_actor_details();
		}
		
	}
	 
	 
	 Integer getcorrectvalue() {
		 
		 boolean correctvalue=false;
		 Integer returnvalue=0;
		 while(!correctvalue) {
			 try {
				 returnvalue=Integer.parseInt(in.nextLine());
				 if (returnvalue>=0) {
					 correctvalue=true;
					 }
			 }
			 catch( NumberFormatException ex) {
				 System.out.println("\n Error!!!n Please enter a valid no");
				 correctvalue=false;
				 
			 }
		 }
		 return returnvalue;
	 }
	 
	 Integer returnActorNo() {
		 return actorNo;
	 }
	 
	 
	 
	void show_scene() {
		
		for(int j=0;j<act.length;j++) {
		act[j].show_actor_details();
		}
		
	}
	public static void main(String[] arg)
	{
		Test ob=new Test();
		ob.scene();
		ob.show_scene();
		
	}
	
	
}
