package learningJava;

public class Dog {
	Dog() {
	System.out.println("this is overloaded default constructors");
}
	Dog(String name) {
	System.out.println("The passed name is :"+name);	
}
public static void main(String[] arg) {
	Dog ob=new Dog();
	Dog ob1=new Dog ("Tommy");
	System.out.println("both constructors have been called");
	
}
}
