

package learningJava;
import java.util.Scanner;

public class Actors {
	
	private String actorName;
	private String type;
	private String gender;
	
	static Scanner in = new Scanner(System.in);
	 
	
	public void get_actor_details() {
	
		    System.out.println("Enter actor name");
			actorName=in.nextLine();
			System.out.println("Enter the role ");
			type=in.nextLine();
			System.out.println("Enter gender");
			gender=in.nextLine();
					}
	
	
	public void show_actor_details() {
		
			System.out.println("Actor:"+actorName+ "  Playing role of:  "+type);
			System.out.println("gender:"+gender);
	}

	public static void main(String[] args) {
	Actors act=new Actors();
	act.get_actor_details();
	act.show_actor_details();
	
	

	}

}
